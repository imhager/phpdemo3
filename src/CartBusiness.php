<?php

/**
 * Created by PhpStorm.
 * User: wangzhonghai
 * Date: 2016/2/2
 * Time: 14:46
 */
class CartBusiness
{
    var $items;  // 购物车中的物品

    // 将 $num 个 $artnr 物品加入购物车

    function add_item($artnr, $num)
    {
        $this->items[$artnr] += $num;
    }

    // 将 $num 个 $artnr 物品从购物车中取出

    function remove_item($artnr, $num)
    {
        if ($this->items[$artnr] > $num) {
            $this->items[$artnr] -= $num;
            return true;
        } elseif ($this->items[$artnr] == $num) {
            unset($this->items[$artnr]);
            return true;
        } else {
            return false;
        }
    }
}