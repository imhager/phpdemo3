<?php
include_once '../src/CartBusiness.php';

/**
 * Created by PhpStorm.
 * User: wangzhonghai
 * Date: 2016/2/2
 * Time: 14:45
 */
class Test extends PHPUnit_Framework_TestCase
{

    /**
     *
     */
    function test_cart()
    {
        $cart = new CartBusiness;
        $cart->items = array("a" => 111, "b" => 222);
        $cart->add_item("a", 1);

        //$another_cart = new Cart;
        $cart->add_item("b", 3);

        foreach ($cart->items as $key => $value) {
            echo $key . ':' . $value;
            echo "\r\n";
        }
    }
}

?>
